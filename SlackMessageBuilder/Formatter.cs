﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlackMessageBuilder
{
    public static class Formatter
    {
        public static string Bold(string message)
        {
            return $"*{message}*";
        }

        public static string Italic(string message)
        {
            return $"_{message}_";
        }

        public static string NewLine => Environment.NewLine;

        public static string InlineCode(string message)
        {
            return $"`{message}`";
        }

        public static string CodeBlock(string message)
        {
            return $"```{NewLine}{message}{NewLine}```";
        }
    }
}
