﻿using Newtonsoft.Json;
using SlackMessageBuilder.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlackMessageBuilder.Objects
{
    public class SlackText : SlackItemBase
    {
        [JsonConstructor]
        public SlackText(string Key, string Value) : base()
        {
            this.Key = Key;
            this.Value = Value;
            this.ValueType = typeof(string);
        }

        public void SetValue(string Value)
        {
            base.SetValue(Value);
        }
    }
}
