﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlackMessageBuilder.Objects
{
    public class SlackContainerList : SlackItemBase
    {
        public SlackContainerList(string Key, List<SlackContainer> Value) : base()
        {
            this.Key = Key;
            this.Value = Value;
            this.ValueType = typeof(List<SlackContainer>);
        }

        public void AddContainer(SlackContainer container)
        {
            ((List<SlackContainer>)Value).Add(container);
        }

        public List<SlackContainer> GetContainer()
        {
            return ((List<SlackContainer>)Value);
        }
    }
}
