﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlackMessageBuilder.Objects
{
    public class SlackBool : SlackItemBase
    {
        public SlackBool(string Key, bool Value) : base()
        {
            this.Key = Key;
            this.Value = Value;
            this.ValueType = typeof(bool);
        }
    }
}
