﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlackMessageBuilder.Objects
{
    public class SlackContainer : List<SlackItemBase>
    {
        public JObject ToJObject()
        {
            JObject obj = new JObject();
            foreach (var part in this)
            {
                if (part.GetType() == typeof(SlackContainerList))
                {
                    JArray lObj = new JArray();
                    foreach (SlackContainer container in ((SlackContainerList)part).GetContainer())
                        lObj.Add(container.ToJObject());
                    obj[part.Key] = lObj;
                }
                else
                {
                    obj[part.Key] = JToken.FromObject(part.Value);
                }
            }
            return obj;
        }

        public override string ToString()
        {
            return ToJObject().ToString();
        }
    }
}
