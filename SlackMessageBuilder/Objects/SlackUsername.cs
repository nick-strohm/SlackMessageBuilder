﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlackMessageBuilder.Objects
{
    public class SlackUsername : SlackText
    {
        public SlackUsername(string Text) : base("username", Text) { }
    }
}
