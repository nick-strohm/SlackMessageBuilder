﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlackMessageBuilder.Objects
{
    public class SlackItemBase
    {
        public string Key { get; internal set; }
        public object Value { get; internal set; }
        [JsonIgnore]
        public Type ValueType { get; internal set; }

        public virtual void SetValue(object Value)
        {
            this.Value = Value;
        }

        public string ToString()
        {
            return ToJson().ToString();
        }

        public JObject ToJson()
        {
            JObject obj = new JObject();
            obj[Key] = JToken.FromObject(Value);
            return obj;
        }
    }
}
