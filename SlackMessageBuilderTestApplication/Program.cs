﻿using SlackMessageBuilder;
using SlackMessageBuilder.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlackMessageBuilderTestApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            SlackMessage message = new SlackMessage();
            message.Add(new SlackText("textusername", "TiaqoY0"));
            message.Add(new SlackUsername("TiaqoY0"));
            message.Add(new SlackContainerList("attachments", SampleAttachmentContainers(4)));
            System.IO.File.WriteAllText("message.json", message.ToJObject().ToString());

            while (true)
                Console.ReadLine();
        }

        public static List<SlackContainer> SampleAttachmentContainers(int n = 2)
        {
            List<SlackContainer> attachments = new List<SlackContainer>();
            for (int i = 0; i < n; i++)
            {
                SlackContainer attachment = new SlackContainer();
                attachment.Add(new SlackText("title", LoremNET.Lorem.Sentence(5)));
                attachment.Add(new SlackContainerList("fields", SampleFieldContainers(3)));
                attachments.Add(attachment);
            }
            return attachments;
        }

        public static List<SlackContainer> SampleFieldContainers(int n = 2)
        {
            List<SlackContainer> fields = new List<SlackContainer>();
            for (int i = 0; i < n; i++)
            {
                SlackContainer field = new SlackContainer();
                field.Add(new SlackText("title", LoremNET.Lorem.Sentence(2)));
                field.Add(new SlackText("value", LoremNET.Lorem.Words(1)));
                field.Add(new SlackBool("short", ((i % 2) == 1 ? true : false)));
                fields.Add(field);
            }
            return fields;
        }
    }
}
